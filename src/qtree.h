#pragma once

#include <functional>
#include <utility>
#include <vector>
#include <array>

namespace stnKrisna {
    // Node access helper
    constexpr std::size_t NorthWest() { return 0; }
    constexpr std::size_t NorthEast() { return 1; }
    constexpr std::size_t SouthEast() { return 2; }
    constexpr std::size_t SouthWest() { return 3; }

    /// Rectangle containing x, y, w, and h.
    /// x & y is the starting coordinate
    /// w & h is the width and height of the rect respectively
    /// Rect origin at top left
    template<typename T>
    class Rect {
    public:
        T x;
        T y;
        T w;
        T h;

        constexpr T getLeft () const noexcept { return x; }
        constexpr T getRight () const noexcept { return x + w; }
        constexpr T getTop () const noexcept { return y; }
        constexpr T getBottom () const noexcept { return y + h; }

        constexpr bool canContain (const Rect &b) const noexcept {
            return (getLeft() <= b.getLeft() &&
                    getRight() >= b.getRight() &&
                    getTop() <= b.getTop() &&
                    getBottom() >= b.getBottom());
        }

        constexpr bool intersects (const Rect &b) const noexcept {
            return (this->getLeft() < b.getRight() &&
                    this->getRight() > b.getLeft() &&
                    this->getBottom() > b.getTop() &&
                    this->getTop() < b.getBottom());
        }
    };
    
    template<typename T>
    Rect<T> subdivide (const Rect<T>& area, uint8_t segment) {
        switch (segment) {
            case NorthWest(): {
                return { area.x, area.y,
                    area.w / static_cast<T>(2), area.h / static_cast<T>(2) };
            }

            case NorthEast(): {
                return { area.x + (area.w / static_cast<T>(2)), area.y,
                    area.w / static_cast<T>(2), area.h / static_cast<T>(2) };
            }

            case SouthEast(): {
                return { area.x + (area.w / static_cast<T>(2)), area.y + (area.h / static_cast<T>(2)),
                    area.w / static_cast<T>(2), area.h / static_cast<T>(2) };
            }

            default:
            case SouthWest(): {
                return { area.x, area.y + (area.h / static_cast<T>(2)),
                    area.w / static_cast<T>(2), area.h / static_cast<T>(2) };
            }
        }
    }

    template<typename T, std::size_t segment>
    Rect<T> subdivide (const Rect<T>& area) {
        return subdivide(area, segment);
    }
    
    template <typename ElementType, typename TCoordinate>
    class QTree_node {
    public:
        QTree_node(Rect<TCoordinate> rect) {
            this->bounds = {rect.x, rect.y, rect.w, rect.h};
        }
        
        /// Vector of reference type to the element in the node
        using ElementContainer = std::vector<std::reference_wrapper<ElementType>>;

        /// Get node bounding rect
        constexpr const Rect<TCoordinate> getNodeBounds () {
            return bounds;
        }
        
        /// Check if the current node can contain a rect
        virtual bool canContain (const Rect<TCoordinate> & rect) {
            return bounds.canContain(rect);
        }
        
        /// Check if the current node can contain a rect
        virtual bool canContain (TCoordinate x, TCoordinate y, TCoordinate w, TCoordinate h) {
            Rect<TCoordinate> area = {x, y, w, h};

            return canContain(area);
        }
        
        /// Add new element with its bounding
        virtual bool push (ElementType element, const Rect<TCoordinate> & bounding) {
            // Check if the new element bounding can be contained in this node
            if (!canContain(bounding)) {
                return false;
            }
            
            elements.emplace_back(std::move(element), std::move(bounding));
            
            return true;
        }
        
        /// Add new element with its bounding
        virtual bool push (ElementType element, TCoordinate x, TCoordinate y, TCoordinate w, TCoordinate h) {
            Rect<TCoordinate> bounding = {x, y, w, h};
            
            return push(element, bounding);
        }

    protected:
        /// Get elements in the current node then store it to ElementContainer
        virtual void getElements (ElementContainer & elementsContainer) {
            // Push all node element to the container
            for (const auto& e : elements) {
                elementsContainer.emplace_back(const_cast<ElementType&>(e.first));
            }
        }

        /// Get elements that is inside a given area then store it to ElementContainer
        virtual void getElements (ElementContainer & elementsContainer, TCoordinate x, TCoordinate y, TCoordinate w, TCoordinate h) {
            // Create temporary area class
            Rect<TCoordinate> area = {x, y, w, h};
            getElements(elementsContainer, area);
        }
  
        /// Get elements that is inside a given area then store it to ElementContainer
        virtual void getElements (ElementContainer & elementsContainer, const Rect<TCoordinate> area) {
            for (const auto& e : elements) {
                if (area.intersects(e.second)) {
                    elementsContainer.emplace_back(const_cast<ElementType&>(e.first));
                }
            }
        }
        
        /// Remove an element from the node
        virtual bool pop (ElementType element, const Rect<TCoordinate> area) {
            uint32_t index = 0;
            for (const auto& e : elements) {
                if (e.first == element && area.intersects(e.second)) {
                    elements.erase(elements.begin() + index);
                    return true;
                }
                ++index;
            }
            return false;
        }
  
        /// Remove an element from the node
        virtual bool pop (ElementType element, TCoordinate x, TCoordinate y, TCoordinate w, TCoordinate h) {
            // Create temporary area class
            Rect<TCoordinate> area = {x, y, w, h};
            return pop(element, area);
        }
  
        virtual uint32_t size () {
            return this->elements.size();
        }
        
        virtual bool empty () {
            return this->elements.size() == 0;
        }
        
        /// Clear the container
        virtual void clear () {
            this->elements.clear();
        }
    
    private:
        // Create child wrapper so that we don't have to rewrite
        using ChildWrapper = std::pair<ElementType, Rect<TCoordinate>>;
        
        // Store node bound
        Rect<TCoordinate> bounds;

        // Store element in the node
        std::vector<ChildWrapper> elements;
    };

    /// Initialize a new quadtree
    template<typename ElementType, typename TCoordinate, std::size_t Depth>
    class Qtree : public QTree_node<ElementType, TCoordinate> {
    public:
        // Allow parent tree to access curent tree's private members
        friend class Qtree<ElementType, TCoordinate, Depth + 1>;
        
        using Leaf = std::unique_ptr<Qtree<ElementType, TCoordinate, Depth - 1>>;
        
        using QTree_ElementContainer = typename QTree_node<ElementType, TCoordinate>::ElementContainer;
        
        /// Create new tree
        inline Qtree(Rect<TCoordinate> bounds) : QTree_node<ElementType, TCoordinate>(bounds) {
            this->initLeafs();
            memberCount = 0;
        }
        
        /// Create new tree
        inline Qtree(TCoordinate x, TCoordinate y, TCoordinate w, TCoordinate h) : QTree_node<ElementType, TCoordinate>({x, y, w, h}) {
            this->initLeafs();
            memberCount = 0;
        }
        
        /// Get the depth of the current tree. The smaller the depth, the further it is from the root tree
        std::size_t getCurrentTreeDepth () {
            return Depth;
        }
        
        /// Push new element to the quadtree
        bool push (ElementType element, const Rect<TCoordinate> & bounding) {
            // Base case. Tree cant contain the new element with its current bounding
            if (!this->canContain(bounding)) {
                return false;
            }
            
            // get the bounds of the current tree
            const Rect<TCoordinate> treeBound = this->getNodeBounds();
            
            // Store directional index
            unsigned char index = 0;
            
            // Find leaf node that can contain the current element with its current bounding
            for (const auto& leaf : leafs) {
                // Get subdivision area
                Rect<TCoordinate> subdivSegment = subdivide<TCoordinate>(treeBound, index);
                if (subdivSegment.canContain(bounding)) {
                    // Check if the tree nodes has been created. Create new leaf nodes when no existing leafs has been created.
                    if (leaf == nullptr) {
                        createLeafs();
                    }
                    
                    if (leaf->push(std::move(element), std::move(bounding))) {
                        ++memberCount;
                        return true;
                    }
                }
                ++index;
            }
            
            // No leaf can contain this element with the current bounding. Store here
            bool success = QTree_node<ElementType, TCoordinate>::push(std::move(element), std::move(bounding));
            if (success)
                ++memberCount;
            
            return success;
        }
        
        bool push (ElementType element, TCoordinate x, TCoordinate y, TCoordinate w, TCoordinate h) {
            Rect<TCoordinate> area = {x, y, w, h};
            return this->push(element, area);
        }
        
        /// Get all elements in the tree
        void getElements (QTree_ElementContainer & elementContainer) {
            // Get elements in the current tree
            QTree_node<ElementType, TCoordinate>::getElements(elementContainer);
            
            // Return early if the current tree's member is 0 or one of the leaf is nullptr
            if (empty() || leafs[0] == nullptr)
                return;
            
            // Then get all elements from the leaf nodes
            for (const auto& leaf : leafs) {
                if (leaf != nullptr)
                    leaf->getElements(elementContainer);
            }
        }
        
        /// Get all elements in the tree inside a search boundry
        void getElements (QTree_ElementContainer & elementContainer, const Rect<TCoordinate> boundary) {
            // Get elements in the current tree that may not be contained in any leaf
            QTree_node<ElementType, TCoordinate>::getElements(elementContainer, boundary);
            
            // Return early if the current tree's member is 0 or one of the leaf is nullptr
            if (empty() || leafs[0] == nullptr)
                return;
            
            // Then get all elements from the leaf nodes
            for (const auto& leaf : leafs) {
                // The current leaf node can contain the boundary. Add leaf elements to the element container
                if (leaf->canContain(boundary)) {
                    leaf->getElements(elementContainer, boundary);
                    return;
                }
                
                // Current leaf node is contained inside the boundary
                if (boundary.canContain(leaf->getNodeBounds())) {
                    leaf->getElements(elementContainer);
                    continue;
                }
                
                // Default behaviour. Boundary is intersecting all (or some) leaf
                leaf->getElements(elementContainer, boundary);
            }
        }
        
        /// Get all elements in the tree inside a search boundry
        void getElements (QTree_ElementContainer & elementContainer, TCoordinate x, TCoordinate y, TCoordinate w, TCoordinate h) {
            Rect<TCoordinate> boundary = {x, y, w, h};
            this->getElements(elementContainer, boundary);
        }
        
        /// Remove an element from the tree within a search area
        bool pop (ElementType element, const Rect<TCoordinate> area) {
            bool didPop = false;
            bool allLeafEmpty = false;
            
            // Search and remove the element fromm the current tree if it exist here
            if (QTree_node<ElementType, TCoordinate>::pop(element, area)) {
                didPop = true;
            } else if (leafs[0] != nullptr) {
                // Search and remove the element from each leaf node
                for (const auto& leaf : leafs) {
                    // The current leaf node can contain the boundary. Add leaf elements to the element container
                    if (leaf->canContain(area)) {
                        didPop |= leaf->pop(element, area);
                        break;
                    }
                    
                    // Current leaf node is contained inside the boundary
                    if (area.canContain(leaf->getNodeBounds())) {
                        didPop |= leaf->pop(element, area);
                        continue;
                    }
                    
                    // Default behaviour. Boundary is intersecting all (or some) leaf
                    didPop |= leaf->pop(element, area);
                    
                    // Check if the current leaf is empty
                    allLeafEmpty &= leaf->empty();
                }
            }
            
            if (didPop) {
                --this->memberCount;
                
                // Remove all leaf if all of the leafs are empty
                if (allLeafEmpty) {
                    freeAllLeaf();
                }
            }
            
            return didPop;
        }
        
        /// Remove an element from the tree within a search area
        bool pop (ElementType element, TCoordinate x, TCoordinate y, TCoordinate w, TCoordinate h) {
            Rect<TCoordinate> boundary = {x, y, w, h};
            return this->pop(element, boundary);
        }
        
        /// Update the boundary of an element
        /// Return false if the element was not previously inserted
        bool update (ElementType element, const Rect<TCoordinate> oldBoundary, const Rect<TCoordinate> newBoundary) {
            // To update the bounding box of an element, simmply remove the element...
            if (!pop(element, oldBoundary)) {
                return false;
            }
            
            // Then add the element back to the tree
            return push(element, newBoundary);
        }
        
        /// Update the boundary of an element
        /// Return false if the element was not previously inserted
        bool update (ElementType element, TCoordinate oldX, TCoordinate oldY, TCoordinate oldW, TCoordinate oldH, TCoordinate newX, TCoordinate newY, TCoordinate newW, TCoordinate newH) {
            Rect<TCoordinate> oldBound = {oldX, oldY, oldW, oldH};
            Rect<TCoordinate> newBound = {newX, newY, newW, newH};
            
            return update(element, oldBound, newBound);
        }
        
        /// Update the boundary of an element.
        /// Insert as new element if the element was not found.
        /// Return false if the element was failed to be inserted.
        bool updateOrInsert (ElementType element, const Rect<TCoordinate> oldBoundary, const Rect<TCoordinate> newBoundary) {
            pop(element, oldBoundary);
            return push(element, newBoundary);
        }
        
        /// Update the boundary of an element.
        /// Insert as new element if the element was not found.
        /// Return false if the element was failed to be inserted.
        bool updateOrInsert (ElementType element, TCoordinate oldX, TCoordinate oldY, TCoordinate oldW, TCoordinate oldH, TCoordinate newX, TCoordinate newY, TCoordinate newW, TCoordinate newH) {
            Rect<TCoordinate> oldBound = {oldX, oldY, oldW, oldH};
            Rect<TCoordinate> newBound = {newX, newY, newW, newH};
            
            return updateOrInsert(element, oldBound, newBound);
        }
        
        /// Returns the number of elements in the current tree
        uint32_t size () {
            return this->memberCount;
        }
        
        /// Check whether the current tree is empty or not
        bool empty () {
            return this->memberCount == 0;
        }
        
        /// Erase all elements from the container
        void clear () noexcept {
            // Clear elements from the current tree
            QTree_node<ElementType, TCoordinate>::clear();
            
            // Return early if the current tree's member is 0 or one of the leaf is nullptr
            if (empty() || leafs[0] == nullptr)
                return;
            
            // Clear elements from leaf nodes
            for (const auto& leaf : leafs) {
                leaf->clear();
            }
            
            // Reset counter
            this->memberCount = 0;
        }
        
    private:
        /// Initialize leaf nodes
        inline void createLeafs() {
            const Rect<TCoordinate> treeBound = this->getNodeBounds();
            
            // Init north west leaf node
            leafs[NorthWest()].reset(
                new Qtree<ElementType, TCoordinate, Depth - 1>( subdivide<TCoordinate, NorthWest()>(treeBound) )
            );
            
            // Init north east leaf node
            leafs[NorthEast()].reset(
                new Qtree<ElementType, TCoordinate, Depth - 1>( subdivide<TCoordinate, NorthEast()>(treeBound) )
            );
            
            // Init south west leaf node
            leafs[SouthWest()].reset(
                new Qtree<ElementType, TCoordinate, Depth - 1>( subdivide<TCoordinate, SouthWest()>(treeBound) )
            );
            
            // Init south east leaf node
            leafs[SouthEast()].reset(
                new Qtree<ElementType, TCoordinate, Depth - 1>( subdivide<TCoordinate, SouthEast()>(treeBound) )
            );
            
        }
        
        /// Initialize leaf nodes as a nullptr
        inline void initLeafs () {
            for (int i = 0; i < 4; ++i) {
                leafs[i] = nullptr;
            }
        }
        
        /// Free memory of all the leaf nodes
        inline void freeAllLeaf () {
            leafs[NorthWest()].release();
            leafs[NorthEast()].release();
            leafs[SouthWest()].release();
            leafs[SouthEast()].release();
            
            // Set all leaf to nullptr
            initLeafs();
        }
        
        std::array<Leaf, 4> leafs;
        
        /// Running count of the member of this tree
        uint32_t memberCount;
    };

    /// Quadtree with depth of 0
    template<typename ElementType, typename TCoordinate>
    class Qtree<ElementType, TCoordinate, 0> : public QTree_node<ElementType, TCoordinate> {
    public:
        friend class Qtree<ElementType, TCoordinate, 1>;
        
        /// Constructor that does nothing since depth 0 has no leaf
        inline Qtree(Rect<TCoordinate> bounds) : QTree_node<ElementType, TCoordinate>(bounds) {}
    };
    
    // Create typedefs so it'll be easy to use
    /// Create a new tree of type T with a depth of maxDepth
    template<typename T, std::size_t maxDepth>
    using QuadtreeDepth = Qtree<T, float, maxDepth>;
    
    /// Create a new tree of type T
    template<typename T>
    using Quadtree = Qtree<T, float, 10>;
}
