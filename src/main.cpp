//
//  main.cpp
//  QTree_xcode
//
//  Created by Stanislaus Krisna on 19/10/20.
//

#include "qtree.h"
#include <iostream>

using Qtree_int = stnKrisna::Quadtree<int>;

void getElement_1 (Qtree_int * qtree) {
    // Get element
    Qtree_int::QTree_ElementContainer treeElements;
    qtree->getElements(treeElements);
    
    // Print elements
    for (int element : treeElements) {
        std::cout << element << " ";
    }
    std::cout << std::endl;
}

void getElement_2 (Qtree_int * qtree) {
    // Get element
    Qtree_int::QTree_ElementContainer treeElements;
    qtree->getElements(treeElements, 0, 0, 5, 5);
    
    // Print elements
    for (int element : treeElements) {
        std::cout << element << " ";
    }
    std::cout << std::endl;
}

void getElement_3 (Qtree_int * qtree) {
    // Get element
    Qtree_int::QTree_ElementContainer treeElements;
    stnKrisna::Rect<float> boundary = {4, 4, 7, 7};
    qtree->getElements(treeElements, boundary);
    
    // Print elements
    for (int element : treeElements) {
        std::cout << element << " ";
    }
    std::cout << std::endl;
}

void getElement_4 (Qtree_int * qtree) {
    // Get element
    Qtree_int::QTree_ElementContainer treeElements;
    stnKrisna::Rect<float> boundary = {5.2, 4, 7, 7};
    qtree->getElements(treeElements, boundary);
    
    // Print elements
    for (int element : treeElements) {
        std::cout << element << " ";
    }
    std::cout << std::endl;
}

void printTreeContent (Qtree_int * qtree) {
    // Is empty?
    std::cout << "Tree empty: " << qtree->empty() << std::endl;
    
    // Get element count
    std::cout << "Element count: " << qtree->size() << std::endl;
}

int main(int argc, const char * argv[]) {
    Qtree_int * qtree = new Qtree_int(0, 0, 10, 10);
    
    // Print tree content
    printTreeContent(qtree);
    
    // Add new element
    qtree->push(0, 0, 0, 1, 1);
    qtree->push(1, 1, 0, 1, 1);
    qtree->push(2, 4, 0, 1, 1);
    qtree->push(3, 7, 0, 1, 1);
    
    qtree->push(4, 0, 7, 1, 1);
    qtree->push(5, 1, 4, 1, 1);
    qtree->push(6, 4, 1, 1, 1);
    qtree->push(7, 7, 0, 1, 1);
    
    qtree->push(8, 5, 6, 1, 1);
    
    // Print tree content
    printTreeContent(qtree);
    
    // Get elements
    getElement_1(qtree);
    getElement_2(qtree);
    getElement_3(qtree);
    getElement_4(qtree);
    
    // Remove element
    std::cout << (qtree->pop(8, 5, 5, 2, 2) ? "8 popped" : "8 not found") << std::endl;
    
    // Update an element
    qtree->update(7, 7, 0, 1, 1, 5, 6, 1, 1);
    getElement_4(qtree);
    
    // Print tree content
    printTreeContent(qtree);
    
    // Empty the tree
    qtree->clear();
    
    // Print tree content
    printTreeContent(qtree);
    
    std::cout << "Hello, World!\n";
    return 0;
}
