# QTree
**A single header quad tree library for C++ built with STL.**

## How to use:

### Create new tree
```c++
// Create typedef of Qtree of type int
// The Quadtree type will create a quadtree with the max depth of 10 nodes 
using Qtree_int = stnKrisna::Quadtree<int>;

// Initialize the quadtree
Qtree_int * qtree = new Qtree_int(0, 0, 10, 10);
```
Or, if you need to create a tree with more (or less) depth, you can do this:
```c++
#define TREE_DEPTH 5

// Create typedef of Qtree of type int
using Qtree_int = stnKrisna::QuadtreeDepth<int, TREE_DEPTH>;

// Initialize the quadtree
Qtree_int * qtree = new Qtree_int(0, 0, 10, 10);
```

### Push new element to an existing tree
```c++
// Simply call the push function
qtree->push(0, 0, 0, 1, 1);

/*
// Or this
stnKrisna::Rect<float> newElementBounding = {0, 0, 1, 1};
qtree->push(0, newElementBounding); 
*/
```

### Remove element from the tree
```c++
// Simply call the pop function
qtree->pop(0, 0, 0, 1, 1);

/*
// Or this
stnKrisna::Rect<float> newElementBounding = {0, 0, 1, 1};
qtree->pop(0, newElementBounding); 
*/
```

### Change bounding box of an existing element in the tree
```c++
// Simply call the update function
// Move element 7 from x:7, y:0, w:1, h:1 to x:5, y:6, w:1, h:1
qtree->update(7, 7, 0, 1, 1, 5, 6, 1, 1);

/*
// Or this
stnKrisna::Rect<float> original = {7, 0, 1, 1};
stnKrisna::Rect<float> changeTo = {5, 6, 1, 1};
qtree->update(7, original, changeTo); 
*/
```
Or, if you want to insert on failed update, you do this:
```c++
qtree->updateOrInsert(7, 7, 0, 1, 1, 5, 6, 1, 1);

/*
// Or this
stnKrisna::Rect<float> original = {7, 0, 1, 1};
stnKrisna::Rect<float> changeTo = {5, 6, 1, 1};
qtree->updateOrInsert(7, original, changeTo); 
*/
```
